﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TG_Enemy : MonoBehaviour
{
    public bool isClaimed;
    public bool isDead;

    public SpriteRenderer spriteRenderer;

    public void Spawn()
    {
        isClaimed = false;
        isDead = false;

        this.gameObject.SetActive(true);
        spriteRenderer.transform.localScale = Vector3.zero;
        spriteRenderer.transform.DOScale(0.3f, 1f);
        spriteRenderer.DOFade(1.0f, 0.0f);
    }

    public void Die()
    {
        spriteRenderer.DOFade(0.0f, 1.0f).OnComplete(() => { isDead = true; this.gameObject.SetActive(false); }).SetDelay(1.0f);
    }
}
