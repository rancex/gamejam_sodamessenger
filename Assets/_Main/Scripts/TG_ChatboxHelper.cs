﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TG_ChatboxHelper : MonoBehaviour
{
    public ScrollRect chatScrollRect;

    public void UpdateChatboxScroll() 
    {
        StartCoroutine(ApplyScrollPosition(chatScrollRect, 0.0f));
    }

    IEnumerator ApplyScrollPosition(ScrollRect sr, float verticalPos)
    {
        yield return new WaitForEndOfFrame();
        sr.verticalNormalizedPosition = verticalPos;
        LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)sr.transform);
    }
}
