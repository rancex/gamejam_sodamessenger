﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TG_ParalaxManager : MonoBehaviour
{
    public float startingPos;
    public List<SpriteRenderer> paralaxBGObjects;

    public float speed = 5f;

    public bool isMoving;

    // Start is called before the first frame update
    void Start()
    {
        Vector3 lastPosition = paralaxBGObjects[0].transform.position;
        Vector3 lastSize = (paralaxBGObjects[0].bounds.max - paralaxBGObjects[0].bounds.min);
    }

    // Update is called once per frame
    void Update()
    {
        if (isMoving)
        {
            float movement = speed * Time.deltaTime;

            for (int i = 0; i < paralaxBGObjects.Count; i++)
            {

                //paralaxBGObjects[i].transform.DOLocalMoveX(paralaxBGObjects[i].transform.localPosition.x - speed, 0.0f);
                paralaxBGObjects[i].transform.localPosition = new Vector3(
                    paralaxBGObjects[i].transform.localPosition.x - movement,
                    paralaxBGObjects[i].transform.localPosition.y,
                    paralaxBGObjects[i].transform.localPosition.z);

                if (paralaxBGObjects[i].transform.localPosition.x <= -startingPos)
                {
                    float xPos;
                    if (i == 0)
                    {
                        xPos = paralaxBGObjects[1].transform.localPosition.x + startingPos - movement;
                    }
                    else
                    {
                        xPos = paralaxBGObjects[0].transform.localPosition.x + startingPos;
                    }

                    //paralaxBGObjects[i].transform.DOLocalMoveX(startingPos, 0.0f);
                    paralaxBGObjects[i].transform.localPosition = new Vector3(
                        xPos,
                        paralaxBGObjects[i].transform.localPosition.y,
                        paralaxBGObjects[i].transform.localPosition.z);
                }
            }
        }
    }

    public void StartMoving()
    {
        isMoving = true;
    }

    public void StopMoving()
    {
        isMoving = false;
    }
}
