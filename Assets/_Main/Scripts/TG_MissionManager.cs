﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TG_MissionManager : MonoBehaviour
{
    public float currentMissionProgress = 0;

    public float maxMissionProgress = 5;

    public TG_ProgressManager progressManager;
    public TG_ParalaxManager paralaxManager;
    public TG_BattleManager battleManager;
    public TG_QuestManager questManager;

    public float progressIncreaseTime;

    public GameObject missionComplete;
    public Button missionCompleteButton;
    public Button backButton;

    public Text expText;

    public Text waveText;

    // Start is called before the first frame update
    void Start()
    {
        waveText.text = "WAVE 1 / " + maxMissionProgress;

        expText.text = TG_Static.exp.ToString();

        TG_AudioManager.Instance.PlayBattleBgm();

        currentMissionProgress = 0;

        if (TG_Static.progressLevel == 1)
        {
            StartTutorialQuest();
        }
        else
        {
            StartQuest_1();
        }

        StartMovingToNextCheckpoint();

        missionCompleteButton.onClick.AddListener(BackToMenu);
        backButton.onClick.AddListener(BackToMenu);
    }

    public void StartTutorialQuest()
    {
        questManager.AssignQuest("tutorial","Your First Quest", "Defeat 5 monsters", 5);
    }

    public void StartQuest_1()
    {
        questManager.AssignQuest("quest_1", "Getting Started", "Defeat 10 monsters", 10);
    }

    public void StartMovingToNextCheckpoint()
    {
        if (currentMissionProgress + 1 <= maxMissionProgress)
        {
            paralaxManager.StartMoving();

            DOTween.To(() => currentMissionProgress, x => currentMissionProgress = x, currentMissionProgress + 1, progressIncreaseTime)
                .OnUpdate(UpdateMissionProgressBar)
                .OnComplete(() =>
                {
                    StartCoroutine(DoCurrentCheckpoint());
                }
                );
        }
        else
        {
            missionComplete.gameObject.SetActive(true);
            TG_AudioManager.Instance.PlayVictorySfx();
        }
    }

    public IEnumerator DoCurrentCheckpoint()
    {
        waveText.text = "WAVE " + currentMissionProgress + " / " + maxMissionProgress;

        paralaxManager.StopMoving();

        battleManager.SpawnEnemies();

        yield return new WaitForSeconds(1.0f);
        battleManager.StartBattle(StartMovingToNextCheckpoint, AddQuestProgress);
        //StartMovingToNextCheckpoint();
    }

    public void AddQuestProgress()
    {
        questManager.AddQuestProgress();
        expText.text = (TG_Static.exp + questManager._currentQuestProgress).ToString();
    }

    public void UpdateMissionProgressBar()
    {
        progressManager.UpdateMissionProgressBar(currentMissionProgress / maxMissionProgress);
    }

    public void BackToMenu()
    {
        SceneManager.LoadScene("HQ");
        TG_Static.exp += questManager._currentQuestProgress;
    }
}
