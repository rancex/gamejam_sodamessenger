﻿using UnityEngine;
using System.Collections;

public class MyTools_GenericSingleton<T> : MonoBehaviour where T : Component
{
    private static T instance;
    public static T Instance
    {
        get
        {
            if(instance == null)
            {
                instance = FindObjectOfType<T>();
            }
            return instance;
        }
    }

    public bool singletonDestroyOnLoad = false;

    public virtual void Awake()
    {
        if(instance == null)
        {
            instance = this as T;
            if (!singletonDestroyOnLoad && transform.parent == null)
            {
                DontDestroyOnLoad(this.gameObject);
            }
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
