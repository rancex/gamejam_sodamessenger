﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TG_QuestManager : MonoBehaviour
{
    public int _currentQuestProgress = 0;
    public int _maxQuestProgress = 0;

    public Button questToggleButton;
    public GameObject questPanel;
    public Text questNameText;
    public Text questDescText;
    public Text questProgressText;
    public GameObject questProgressBar;
    public Slider questProgressSlider;

    public Text questCompletedText;

    public bool isQuestDone = false;
    public string _currentQuestID = "";

    // Start is called before the first frame update
    void Start()
    {
        questToggleButton.onClick.AddListener(ToggleQuestWindow);
    }

    public void ToggleQuestWindow()
    {
        questPanel.SetActive(!questPanel.activeSelf);
    }

    public void AssignQuest(string questID, string questName,string questDesc, int maxQuestProgress)
    {
        _currentQuestID = questID;

        _currentQuestProgress = 0;
        _maxQuestProgress = maxQuestProgress;

        questNameText.text = questName;
        questDescText.text = questDesc;

        questProgressText.text = "0 of " + maxQuestProgress.ToString();
        questProgressSlider.value = 0;
        questProgressBar.SetActive(true);
        questCompletedText.gameObject.SetActive(false);
    }

    public void AddQuestProgress()
    {
        _currentQuestProgress++;

        if (_currentQuestProgress >= _maxQuestProgress)
        {
            isQuestDone = true;
            questProgressBar.SetActive(false);
            questCompletedText.gameObject.SetActive(true);
        }
        else
        {
            questProgressText.text = _currentQuestProgress.ToString() + " of " + _maxQuestProgress.ToString();
            questProgressSlider.value = (float)_currentQuestProgress / (float)_maxQuestProgress;
        }
    }
}
