﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TG_MainMenu : MonoBehaviour
{
    public Button startButton;
    public InputField nameInputField;

    // Start is called before the first frame update
    void Start()
    {
        TG_AudioManager.Instance.PlayHQBgm();

        startButton.onClick.AddListener(GoToTutorial);
    }

    public void GoToTutorial()
    {
        TG_Static.progressLevel = 1;
        TG_Static.MCName = nameInputField.text;
        SceneManager.LoadScene("Battle_Scene");
    }
}
