﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TG_Hero : MonoBehaviour
{
    public bool isAttacking;

    public float cooldownTime;

    public float remainingCooldown;

    public Vector3 basePosition;

    public Slider cooldownBarSlider;

    public GameObject cooldownBar;

    // Start is called before the first frame update
    void Start()
    {
        basePosition = this.transform.position;
        remainingCooldown = cooldownTime;
    }

    public void UpdateCooldown()
    {
        if (!isAttacking && remainingCooldown > 0)
        {
            remainingCooldown -= Time.deltaTime;

            float cooldownProgress = (cooldownTime - remainingCooldown) / cooldownTime;

            cooldownBarSlider.value = cooldownProgress;
        }
    }

    public void ResetCooldown()
    {
        remainingCooldown = cooldownTime;
    }
}
