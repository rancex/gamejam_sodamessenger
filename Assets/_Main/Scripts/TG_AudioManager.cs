﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TG_AudioManager : MyTools_GenericSingleton<TG_AudioManager>
{
    [Header("Audio Source")]
    [SerializeField]
    private AudioSource m_BgmSource;
    [SerializeField]
    private AudioSource m_SfxSource;

    [Header("BGM")]
    public AudioClip HQBGM;
    public AudioClip battleBGM;
    public AudioClip sceneBGM;

    [Header("SFX")]
    public AudioClip slashSFX;
    public AudioClip victorySFX;
    public AudioSource BgmSource { get => m_BgmSource; set => m_BgmSource = value; }
    public AudioSource SfxSource { get => m_SfxSource; set => m_SfxSource = value; }

    public void PlayBgm(AudioClip clip)
    {
        if (BgmSource.clip != clip)
        {
            BgmSource.clip = clip;
            BgmSource.Play();
        }
    }

    public void StopBgm()
    {
        BgmSource.Stop();
    }

    public void PlayHQBgm()
    {
        PlayBgm(HQBGM);
    }

    public void PlayBattleBgm()
    {
        PlayBgm(battleBGM);
    }

    public void PlaySceneBgm()
    {
        PlayBgm(sceneBGM);
    }

    public void PlaySfx(AudioClip audioClip, bool playOneShot, bool randomizePitch = false)
    {
        if (audioClip)
        {
            SfxSource.pitch = randomizePitch == true ? Random.Range(1.0f, 1.5f) : 1.0f;

            if (playOneShot)
            {
                SfxSource.PlayOneShot(audioClip);
            }
            else
            {
                SfxSource.clip = audioClip;
                SfxSource.Play();
            }
        }
    }

    public void PlaySlashSfx()
    {
        PlaySfx(slashSFX, true);
    }

    public void PlayVictorySfx()
    {
        PlaySfx(victorySFX, true);
    }
}
