﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TG_BattleManager : MonoBehaviour
{
    public TG_Hero playerCharacter;
    public TG_Hero ragaCharacter;
    public TG_Hero lolCharacter;
    public TG_Hero anantaCharacter;

    public List<TG_Hero> heroList;
    public List<TG_Enemy> enemyList;

    public System.Action _OnBattleEnd;
    public System.Action _OnEnemyKilled;

    public bool isBattling;

    public void Start()
    {
        for (int i = 0; i < enemyList.Count; i++)
        {
            enemyList[i].gameObject.SetActive(false);
            enemyList[i].isDead = true;
            enemyList[i].isClaimed = true;
        }

        if (TG_Static.progressLevel < 3)
        {
            heroList = new List<TG_Hero>() { playerCharacter};

            ragaCharacter.cooldownBar.SetActive(false);
            ragaCharacter.gameObject.SetActive(false);
            lolCharacter.cooldownBar.SetActive(false);
            lolCharacter.gameObject.SetActive(false);
            anantaCharacter.cooldownBar.SetActive(false);
            anantaCharacter.gameObject.SetActive(false);
        }
        else
        {
            heroList = new List<TG_Hero>() { playerCharacter, ragaCharacter, lolCharacter, anantaCharacter };
        }
    }

    public void Update()
    {
        if (isBattling)
        {
            List<TG_Hero> heroWithFinishedCooldown = new List<TG_Hero>();

            for (int i = 0; i < heroList.Count; i++)
            {
                heroList[i].UpdateCooldown();

                if (heroList[i].remainingCooldown <= 0f && !heroList[i].isAttacking) heroWithFinishedCooldown.Add(heroList[i]);
            }

            bool anyEnemyAlive = false;
            for (int i = 0; i < enemyList.Count; i++)
            {
                if (!enemyList[i].isDead) anyEnemyAlive = true;
            }

            if (anyEnemyAlive)
            {
                if (heroWithFinishedCooldown.Count > 0)
                {
                    int random = Random.Range(0, heroWithFinishedCooldown.Count);
                    TG_Hero targetHero = heroWithFinishedCooldown[random];

                    TG_Enemy targetEnemy = GetRandomEnemy();

                    if (targetEnemy != null)
                    {
                        targetHero.isAttacking = true;
                        targetEnemy.isClaimed = true;

                        targetHero.transform.DOMove(targetEnemy.transform.position, 0.5f).OnComplete(() =>
                        {
                            TG_AudioManager.Instance.PlaySlashSfx();
                            _OnEnemyKilled?.Invoke();
                            targetEnemy.Die();
                            targetHero.ResetCooldown();
                            targetHero.transform.DOMove(targetHero.basePosition, 0.5f).OnComplete(() => { targetHero.isAttacking = false; }).SetDelay(0.5f);
                        });
                    }
                }
            }
            else
            {
                isBattling = false;
                EndBattle();
            }
        }
    }

    public TG_Enemy GetRandomEnemy()
    {
        List<TG_Enemy> livingUnclaimedEnemy = new List<TG_Enemy>();

        for (int i = 0; i < enemyList.Count; i++)
        {
            if (!enemyList[i].isDead && !enemyList[i].isClaimed) livingUnclaimedEnemy.Add(enemyList[i]);
        }

        if (livingUnclaimedEnemy.Count > 0)
        {
            int random = Random.Range(0, livingUnclaimedEnemy.Count);
            return livingUnclaimedEnemy[random];
        }


        return null;
    }

    public void StartBattle(System.Action OnBattleEnd, System.Action OnEnemyKilled)
    {
        _OnBattleEnd = OnBattleEnd;
        _OnEnemyKilled = OnEnemyKilled;

        isBattling = true;

        for (int i = 0; i < heroList.Count; i++)
        {
            heroList[i].ResetCooldown();
        }

        /*
        playerCharacter.transform.DOMove(enemyCharacter.transform.position,0.5f).OnComplete(() => {
            playerCharacter.transform.DOMove(basePlayerPosition, 0.5f).OnComplete(EndBattle).SetDelay(0.5f);
        }).SetDelay(2.0f);
        */
    }

    public void SpawnEnemies()
    {
        int enemyAmount = 0;
        if(TG_Static.progressLevel < 3)
        {
            enemyAmount = 1;
        }
        else
        {
            enemyAmount = Random.Range(1, enemyList.Count + 1);
        }

        List<TG_Enemy> tempEnemyList = new List<TG_Enemy>(enemyList);
        List<TG_Enemy> chosenEnemyList = new List<TG_Enemy>();

        for (int i =0; i < enemyAmount; i++)
        {
            int random = Random.Range(0, tempEnemyList.Count);
            chosenEnemyList.Add(tempEnemyList[random]);
            tempEnemyList.RemoveAt(random);
        }

        for(int j = 0;j < chosenEnemyList.Count; j++)
        {
            chosenEnemyList[j].Spawn();
        }
    }

    public void EndBattle()
    {
        _OnBattleEnd?.Invoke();
    }
}
