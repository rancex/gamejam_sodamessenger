﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TG_ChatGroupManager : MonoBehaviour
{
    public GameObject chatGroupPrefab;
    public Transform chatGroupListTransform;

    public Sprite ragaChatSprite;
    public Sprite LOLSprite;
    public Sprite anantaSprite;
    public Sprite togeSprite;

    public List<TG_ChatGroupBox> chatGroupBoxList = new List<TG_ChatGroupBox>();

    // Start is called before the first frame update
    public void Init()
    {
        if (TG_Static.progressLevel == 1)
        {
            AddChatGroupButton("Raga_P", "Hi", 0, ragaChatSprite, delegate { GoToChat("Raga_1_Scene_1"); });
        }
        if (TG_Static.progressLevel == 2)
        {
            AddChatGroupButton("Toge_Guild", "top of my class bish", 5, togeSprite, delegate { GoToChat("Toge_1_Scene_1"); });
        }
        if (TG_Static.progressLevel == 3)
        {
            AddChatGroupButton("Raga_P", "Do you enjoy Mabar so far?", 10, ragaChatSprite, delegate { GoToChat("Raga_2_Scene_1"); });
            AddChatGroupButton("eL_O_eL", "u're actually not bad...", 10, LOLSprite, delegate { GoToChat("LOL_1_Scene_1"); });
            AddChatGroupButton("AnantaRupa", "What do you want to buy?", 10, anantaSprite, delegate { GoToChat("Ananta_1_Scene_1"); });
        }
    }

    public void AddChatGroupButton(string chatTitle, string chatContent, int expNeeded, Sprite chatSprite, System.Action buttonAction)
    {
        TG_ChatGroupBox chatGroupBox = Instantiate(chatGroupPrefab, chatGroupListTransform).GetComponent<TG_ChatGroupBox>();
        chatGroupBox.Init(chatTitle, chatContent, expNeeded, chatSprite, buttonAction);

        chatGroupBoxList.Add(chatGroupBox);
    }

    public void GoToChat(string chatID)
    {
        TG_Static.chatID = chatID;
        SceneManager.LoadScene("Scene_Chat");
    }

    public void RemoveAllChat()
    {
        for(int i = chatGroupBoxList.Count - 1;i >= 0; i--)
        {
            Destroy(chatGroupBoxList[i].gameObject);
            chatGroupBoxList.RemoveAt(i);
        }
    }
}
