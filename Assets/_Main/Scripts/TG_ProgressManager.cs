﻿using Doozy.Engine.Progress;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TG_ProgressManager : MonoBehaviour
{
    public Progressor missionProgressBarProgressor;
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateMissionProgressBar(float value)
    {
        missionProgressBarProgressor.SetProgress(value);
    }
}
