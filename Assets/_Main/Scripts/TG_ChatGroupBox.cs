﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TG_ChatGroupBox : MonoBehaviour
{
    public Text _chatTitleText;
    public Text _chatContentText;

    public Text _expNeededText;

    public Image _chatImage;

    public Button _button;

    public int _expNeeded;
    public string _targetScene;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Init(string chatTitle, string chatContent, int expNeeded, Sprite chatSprite, System.Action buttonAction)
    {
        _chatTitleText.text = chatTitle;
        _chatContentText.text = chatContent;

        _expNeededText.text = expNeeded.ToString();
        _expNeeded = expNeeded;

        _chatImage.sprite = chatSprite;

        _button.onClick.AddListener(delegate { if(TG_Static.exp >= _expNeeded) buttonAction?.Invoke(); });
    }
}
