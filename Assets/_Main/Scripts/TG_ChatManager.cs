﻿using Fungus;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TG_ChatManager : MonoBehaviour
{
    public Flowchart targetFlowchart;

    // Start is called before the first frame update
    void Start()
    {
        TG_AudioManager.Instance.PlaySceneBgm();
        targetFlowchart.SetStringVariable("MC_name", TG_Static.MCName);
        targetFlowchart.ExecuteIfHasBlock(TG_Static.chatID);
    }

    public void FinishChat()
    {
        SceneManager.LoadScene("HQ");
    }
}
