﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TG_HQManager : MonoBehaviour
{
    public TG_ChatGroupManager chatGroupManager;

    public Text expText;
    public Button chatGroupToggleButton;

    public Button battleButton;

    // Start is called before the first frame update
    void Start()
    {
        TG_AudioManager.Instance.PlayHQBgm();

        //check last chat id
        if (TG_Static.chatID == "Raga_1_Scene_1") TG_Static.progressLevel = 2;
        if (TG_Static.chatID == "Toge_1_Scene_1") TG_Static.progressLevel = 3;

        expText.text = TG_Static.exp.ToString();
        chatGroupManager.Init();
        chatGroupToggleButton.onClick.AddListener(ToggleChatGroup);

        battleButton.onClick.AddListener(StartBattle);
    }

    public void ToggleChatGroup()
    {
        chatGroupManager.gameObject.SetActive(!chatGroupManager.gameObject.activeSelf);
    }

    // Update is called once per frame
    void Update()
    {
        Cheat();
    }

    public void StartBattle()
    {
        SceneManager.LoadScene("Battle_Scene");
    }
    public void Cheat()
    {
        if (Input.GetKeyDown(KeyCode.F1))
        {
            chatGroupManager.RemoveAllChat();
            TG_Static.progressLevel = 1;
            chatGroupManager.Init();
        }
        if (Input.GetKeyDown(KeyCode.F2))
        {
            chatGroupManager.RemoveAllChat();
            TG_Static.progressLevel = 2;
            chatGroupManager.Init();
        }
        if (Input.GetKeyDown(KeyCode.F3))
        {
            chatGroupManager.RemoveAllChat();
            TG_Static.progressLevel = 3;
            chatGroupManager.Init();
        }

        if (Input.GetKeyDown(KeyCode.F4))
        {
            TG_Static.exp += 100;
            expText.text = TG_Static.exp.ToString();
        }

        if (Input.GetKeyDown(KeyCode.F10))
        {
            SceneManager.LoadScene("Main Menu");
        }
    }
}
